package com.example.newsfeednew

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class ArticleView : AppCompatActivity(), NewsFeedAdapter.OnItemClickListener {

    private lateinit var articleView: NewsFeedAdapter

    override fun onCreate(savedInstanceState: Bundle?) {


        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        articleView = NewsFeedAdapter(mutableListOf(CardContent("","","","","","","","")),
            this)
        recycleView.adapter = articleView
        recycleView.layoutManager = LinearLayoutManager(this)


    }

    override fun onItemCLick(position: Int, item: CardContent) {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

}