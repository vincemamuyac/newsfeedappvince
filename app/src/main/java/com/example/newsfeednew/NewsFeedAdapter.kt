package com.example.newsfeednew
//promote ui in view to lazy load
//lazy load:
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.news_card.view.*

class NewsFeedAdapter (
    private val listener: OnItemClickListener): RecyclerView.Adapter<NewsFeedAdapter.NewsFeedViewHolder>() {
    class NewsFeedViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)

    private var newsFeedItems:MutableList<CardContent> = mutableListOf() //method returning empty list


    fun addNewsFeedItems(cardContents: MutableList<CardContent>){
        newsFeedItems.addAll(cardContents)
        notifyDataSetChanged()
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsFeedViewHolder {
        return NewsFeedViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.news_card, parent, false))
    }

    override fun onBindViewHolder(holder: NewsFeedViewHolder, position: Int) { //loaded into memory
        val currentContent = newsFeedItems[position]
        holder.itemView.apply {
            cardTitle.text = currentContent.title
            cardContent.text = currentContent.description
            authorCard.text = currentContent.author
            dateCard.text = currentContent.publishedAt
            Glide.with(this)
                    .load(currentContent.urlToImage)
                    .centerCrop()
                    .into(imageContent)


        }
        holder.itemView.newsCardView.setOnClickListener {
            listener.onItemCLick(position, currentContent)
        }
    }

    override fun getItemCount(): Int {
        return newsFeedItems.size
    }

    inner class ViewHolderOnClick(itemView: View):RecyclerView.ViewHolder(itemView), View.OnClickListener{
        val image_content: ImageView = itemView.imageContent
        val card_title: TextView = itemView.cardTitle
        val card_content: TextView = itemView.cardContent
        val author_card: TextView = itemView.authorCard
        val date_card: TextView = itemView.dateCard

        init{
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val position: Int = adapterPosition
            if (position != RecyclerView.NO_POSITION){
                listener.onItemCLick(position, newsFeedItems[position])
            }

        }
    }
    interface OnItemClickListener{
        fun onItemCLick(position: Int, item: CardContent)
    }
}