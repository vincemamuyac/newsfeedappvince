package com.example.newsfeednew.http.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

//news_api_everything_response
//class name is also file name generally
@JsonClass(generateAdapter = true)
data class NewsApiEverythingResponse(
        @Json(name = "articles") val articles: List<ArticleResponse>,
        @Json(name = "status") val status: String,
        @Json(name = "totalResults") val totalResults: Int
)