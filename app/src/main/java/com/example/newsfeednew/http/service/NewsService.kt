package com.example.newsfeednew.http.service

import com.example.newsfeednew.http.response.NewsApiEverythingResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query
//step 1
private const val QUERY_TITLE = "title"
private const val QUERY_DESC = "description"
private const val QUERY_URL = "url"
private const val QUERY_URL_IMAGE = "urlToImage"
private const val QUERY_AUTHOR = "author"
private const val QUERY_PUBLISHED = "publishedAt"
private const val QUERY_CONTENT = "content"
private const val QUERY_SOURCE = "source"
private const val QUERY_API_KEY = "apiKey"
private const val QUERY_TOPIC = "q"

interface NewsService {
    @GET("everything") //ikakabit yung url
    suspend fun getNewsData(
            @Header(QUERY_API_KEY) apiKey: String,
            @Query(QUERY_TOPIC) q: String,
        ): Response<NewsApiEverythingResponse> //gives response
        //read about generics
        //for error 401: auth problem;
        //404: not found
        //500 above: internal server error
}