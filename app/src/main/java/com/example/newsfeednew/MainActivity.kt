package com.example.newsfeednew

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.newsfeednew.http.service.NewsService
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

const val BASE_URL_NEWSAPI = "https://newsapi.org/v2/"
const val BASE_API_KEY = "3011666a0f6044158984eee535ac95a3"
class MainActivity : AppCompatActivity(), NewsFeedAdapter.OnItemClickListener {

    private lateinit var newsFeed: NewsFeedAdapter
    val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL_NEWSAPI)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
    val service = retrofit.create(NewsService::class.java) //implements NewsService interface

    suspend fun loadNews() {
        val response = service.getNewsData(q = "Tesla", apiKey = BASE_API_KEY)
        if (response.isSuccessful){
            val body = response.body()
            if (body != null){
                val cardContents = body.articles.map{CardContent.from(it)}.toMutableList() //it = response
                newsFeed.addNewsFeedItems(cardContents)
            }
        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        newsFeed = NewsFeedAdapter( //from suspend fun loadNews
                this)
        recycleView.adapter = newsFeed
        recycleView.layoutManager = LinearLayoutManager(this)
        lifecycleScope.launch {
            loadNews()  } //coroutinescope launch //longrunning op
        //finished oncreate anyways

    }



    override fun onItemCLick(position: Int, item: CardContent) {
        Toast.makeText(this, "Card $position clicked", Toast.LENGTH_SHORT).show()
        var articleList = mutableListOf<String>()
        startActivity(intent)
    }




}