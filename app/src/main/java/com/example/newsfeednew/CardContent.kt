package com.example.newsfeednew

import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.news_card.view.*
import javax.xml.transform.Source

data class CardContent(
    val author: String? = null,
    val content: String? = null,
    val description: String? = null,
    val publishedAt: String? = null,
    val source: String? = null,
    val title: String? = null,
    val url: String? = null,
    val urlToImage: String? = null
){

    companion object{}

}

fun CardContent.Companion.from(response: NewsApiResponse) : CardContent{

    return CardContent(
        author = response.author,
        content = response.content,
        description = response.description,
        publishedAt = response.publishedAt,
        title = response.title,
        source = response.source,
        url = response.url,
        urlToImage = response.urlToImage

    )
}